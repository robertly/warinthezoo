const STYLE_SCORING = new PIXI.TextStyle({fontFamily: 'Franklin Gothic',fontSize: 50, stroke:0x085163,strokeThickness:10, align:"center", wordWrapWidth:600, fill: "white",letterSpacing:1});
const STYLE_GAME_OVER = new PIXI.TextStyle({fontFamily: 'Franklin Gothic',fontSize: 70, stroke:0x085163,strokeThickness:0, align:"center", wordWrapWidth:600, fill: "white",letterSpacing:1});
const STYLE_GAME_OVER_SCORE = new PIXI.TextStyle({fontFamily: 'Arial',fontSize: 30, stroke:0x085163,strokeThickness:0, align:"center", wordWrapWidth:600, fill: "white",letterSpacing:1});
const STYLE_LEVELING = new PIXI.TextStyle({fontFamily: 'Franklin Gothic',fontSize: 50, stroke:"white",strokeThickness:10, align:"center", wordWrapWidth:600, fill:0x085163,letterSpacing:1});
const STYLE_TITLE = new PIXI.TextStyle({fontFamily: 'Franklin Gothic',fontSize: 36,fill: "white",letterSpacing:1});
const STYLE_SUBTITLE = new PIXI.TextStyle({fontFamily: 'Franklin Gothic',fontSize: 22,fill: "white",letterSpacing:1});
const STYLE_TXT = new PIXI.TextStyle({fontFamily: 'Arial',fontSize: 12,fill: "white",letterSpacing:.5,lineHeight:12, wordWrapWidth:300, align:"center",  wordWrap:true});
const STYLE_BTN = new PIXI.TextStyle({fontFamily: 'Franklin Gothic',fontSize: 22,fill: 0x1099bb,letterSpacing:2,lineHeight:12, wordWrapWidth:100, align:"center",  wordWrap:true});
const STYLE_NEXT = new PIXI.TextStyle({fontFamily: 'Franklin Gothic',fontSize: 16,fill: 0x1099bb,letterSpacing:1});
const STYLE_LOADING = new PIXI.TextStyle({fontFamily: 'Franklin Gothic',fontSize: 22,fill: 0x085163,letterSpacing:1});

export default class FontStyle {
    static get STYLE_TITLE(){ return STYLE_TITLE;}
    static get STYLE_SUBTITLE(){ return STYLE_SUBTITLE;}
    static get STYLE_BTN(){ return STYLE_BTN;}
    static get STYLE_TXT(){ return STYLE_TXT;}
    static get STYLE_NEXT(){ return STYLE_NEXT;}
    static get STYLE_SCORING(){ return STYLE_SCORING;}
    static get STYLE_LEVELING(){ return STYLE_LEVELING;}
    static get STYLE_LOADING(){ return STYLE_LOADING;}
    static get STYLE_GAME_OVER(){ return STYLE_GAME_OVER;}
    static get STYLE_GAME_OVER_SCORE(){ return STYLE_GAME_OVER_SCORE;}
}
