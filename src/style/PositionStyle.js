const POSITION_TITLE = {x:20,y:15};
const POSITION_GRID = {x:80,y:150};
const MARGIN_INFO = 20;
const LEVEL_Y = 340;
const SCORE_Y = 400;
const MAXCHAIN_Y = 460;

export default class PositionStyle {
    static get POSITION_TITLE(){ return POSITION_TITLE;}
    static get POSITION_GRID(){ return POSITION_GRID;}
    static get MARGIN_INFO(){ return MARGIN_INFO;}
    static get LEVEL_Y(){ return LEVEL_Y;}
    static get SCORE_Y(){ return SCORE_Y;}
    static get MAXCHAIN_Y(){ return MAXCHAIN_Y;}
}