import ParticuleObject from "./../plastique/object/ParticuleObject.js";
import SpriteObject from "./../plastique/object/SpriteObject.js";
import RectangleObject from "./../plastique/object/RectangleObject.js";
import CardObject from "./CardObject.js";
import Config from "./../Config.js";
import Command from "./../command/Command.js";
import GameState from "./../state/GameState.js";
import FontStyle from "./../style/FontStyle.js";
import Asset from "./../Asset.js";


export default class GameWorldObject extends PIXI.Container{

  constructor() {
    super();
    this.resultat = "start";
    this.createCrown();
    this.distributeAllCard();
  }


  // *************************** //
  // *****     CROWN      ****** //
  // *************************** //
  createParticule(){
    if(this.particule){
      this.removeChild(this.particule);
      this.particule = null;
    }
    (new Command()).emit(Command.ON_IMPACT);
    this.particule = new ParticuleObject(Asset.ATLAS, Asset.STAR, Asset.PARTICULE_STAR,.5);
    this.addChild(this.particule);
    this.particule.x = 400;
    this.particule.y = 220;
    this.particule.startParticule();
  }

  // *************************** //
  // *****     CROWN      ****** //
  // *************************** //
  createCrown(){
    this.crown = new SpriteObject(Asset.ATLAS, Asset.CROWN);
    this.addChild(this.crown);
    this.crown.alpha = 0;
    this.crown.scale.x = this.crown.scale.y = .5;
  }

  hideCrown(){
    TweenLite.to(this.crown, .25, {alpha:0, y:130 , ease: Quad.easeOut});
  }

  showCrown(isOpponent,d){
    this.crown.x = isOpponent?343:429;
    this.crown.y = 130;
    this.crown.alpha = 0;
    TweenLite.to(this.crown, .25, {alpha:1, y:150 , delay:d, ease: Back.easeOut});
  }

  // **************************************** //
  // *****     INIT // GIVE CARDS      ****** //
  // **************************************** //
  distributeAllCard(){
    this.aCard = [];

    let gameState = new GameState();

    for(let i=0;i<gameState.deckOpponent.length;i++){
      this.distributeCard(i,gameState.deckOpponent[i],i*.25);
    }

    for(let i=0;i<gameState.deckPlayer.length;i++){
      this.distributeCard(i,gameState.deckPlayer[i],i*.25+.12);
    }
    setTimeout(function(){
      (new Command()).emit(Command.IS_READY);
    },(5*.25+.12+1)*1000);

    (new Command()).addListener(Command.ON_NEXT,()=>this.onNext());
  }

  distributeCard(i,cardInfos, delay){
    setTimeout(()=>(new Command()).emit(Command.ON_CARD),delay*1000+350);
    let card = new CardObject(cardInfos);
    this.addChild(card);
    this.aCard.push(card);
    card.putAs("out",cardInfos.isOpponent,i);
    setTimeout(function(){
      card.putAs("deck",cardInfos.isOpponent,i);
    },delay*1000);
  }

  // ************************************ //
  // *****     ON NEXT BUTTON      ****** //
  // ************************************ //
  onNext(){
    (new Command()).emit(Command.WAIT_PLEASE);
    if(this.resultat == "start"){
      this.showNextCards();
      return;
    }

    let gameState = new GameState();
    let card1 = this.getCard(gameState.removeCardOpponent());
    let card2 = this.getCard(gameState.removeCardPlayer());

    if(this.resultat == "equals"){
      this.sentToDeck(true,card1);
      this.sentToDeck(false,card2);
    }else if(this.resultat == "playerwin"){
      this.hideCrown();
      this.sentToDeck(false,card1,card2);
    }else if(this.resultat == "opponentwin"){
      this.hideCrown();
      this.sentToDeck(true,card1,card2);
    }
    this.reOrder();
    setTimeout(()=>(new Command()).emit(Command.ON_CARD),600);
    setTimeout(()=>(new Command()).emit(Command.ON_CARD),630);

    setTimeout(()=>this.showNextCards(),1200);
  }

  reOrder(){
    for(let i=0;i<this.aCard.length;i++){
      this.removeChild(this.aCard[i]);
    }

    let gameState = new GameState();
    for(let i=0;i<gameState.deckOpponent.length;i++){
      let card = this.getCard(gameState.deckOpponent[i]);
      this.addChild(card);
    }

    for(let i=0;i<gameState.deckPlayer.length;i++){
      let card = this.getCard(gameState.deckPlayer[i]);
      this.addChild(card);
    }
  }

  showNextCards(){
    let gameState = new GameState();
    if(gameState.isFinito()){
      this.gameOver();
      return;
    }

    setTimeout(()=>(new Command()).emit(Command.ON_TURN),600);
    setTimeout(()=>(new Command()).emit(Command.ON_TURN),630);
    setTimeout(()=>(new Command()).emit(Command.ON_CARD),100);
    setTimeout(()=>(new Command()).emit(Command.ON_CARD),130);
    let card1 = this.getCard(gameState.getNextCardOpponent());
    let card2 = this.getCard(gameState.getNextCardPlayer());
    card1.putAs("battle",true,0);
    card2.putAs("battle",false,0);

    setTimeout(()=>this.createParticule(),1600);
    setTimeout(()=>this.onNextFinito(),2200);

    if(card1.infos.num == card2.infos.num){
      this.resultat = "equals";
      //equals
    }else if(card1.infos.num == 5 && card2.infos.num == 1 || ((card1.infos.num < card2.infos.num) && !(card1.infos.num == 1 && card2.infos.num == 5))){
      //playerwin
      this.resultat = "playerwin";
      this.showCrown(false,2.3);
    }else {
      //opponentwin
      this.resultat = "opponentwin";
      this.showCrown(true,2.3);
    }
  }

  onNextFinito(){
    (new Command()).emit(Command.IS_READY);
  }

  // ********************************** //
  // *****     SEND TO DECK      ****** //
  // ********************************** //
  sentToDeck(isOpponent,card1,card2=null){
    let gameState = new GameState();
    let d = 1;

    // move cards
    card1.putAs("deck",isOpponent,0);
    if(card2){
      card2.putAs("deck",isOpponent,1);
      d = 2;
    }

    // reorder
    let deck = gameState.deckPlayer;
    if(isOpponent) deck = gameState.deckOpponent;
    for(let i=0;i<deck.length;i++){
      let card = this.getCard(deck[i]);
      card.putAs("deck",isOpponent,i+d);
    }

    // update le deck dans le gamestate
    if(card2){
      if(!isOpponent) gameState.addToPlayerDeck(card2.infos);
      if(isOpponent) gameState.addToOpponentDeck(card2.infos);
    }

    if(!isOpponent) gameState.addToPlayerDeck(card1.infos);
    if(isOpponent) gameState.addToOpponentDeck(card1.infos);
  }

  // ************************* //
  // *****     FCT      ****** //
  // ************************* //
  getCard(cardinfos){
    for(let i=0;i<this.aCard.length;i++){
      if(this.aCard[i].infos.isOpponent == cardinfos.isOpponent && this.aCard[i].infos.num == cardinfos.num) return this.aCard[i];
    }
  }

  gameOver(){
    console.log("GAMEOVER!");
    setTimeout(()=>(new Command()).emit(Command.ON_GAME_OVER),600);
    let gameState = new GameState();
    let b = gameState.doYouWin();
    let txt= b?"you win!!!!":"you lose!!!!";

    let rect = new RectangleObject(Config.STAGE.width,Config.STAGE.height,0,0);
    this.addChild(rect);
    rect.alpha=0;

    let bitmapText = new PIXI.extras.BitmapText(txt, {font: "50px "+Asset.TEXT_BIGWIN, align: "left"});
    this.addChild(bitmapText);
    let colorMatrix = new PIXI.filters.ColorMatrixFilter();
    colorMatrix.hue(b?-85:135);
    bitmapText.filters = [colorMatrix];
    bitmapText.x = Config.STAGE.width/2-bitmapText.textWidth/2;
    bitmapText.y = -75;

    TweenLite.to(rect, .5, {alpha:.88, ease: Quad.easeOut});
    TweenLite.to(bitmapText, 1, {y:200, ease: Bounce.easeOut, delay : .5});
  }
}
