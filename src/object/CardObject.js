import ParticuleObject from "./../plastique/object/ParticuleObject.js";
import PlaneObject from "./../plastique/object/PlaneObject.js";
import SpriteObject from "./../plastique/object/SpriteObject.js";
import RectangleObject from "./../plastique/object/RectangleObject.js";
import Config from "./../Config.js";
import FontStyle from "./../style/FontStyle.js";
import Asset from "./../Asset.js";


const VECTOR_QUALITY = 5;
const W = 78;
const H = 117;

export default class CardObject extends PIXI.Container{

  constructor(cardInfos) {
    super();
    this.infos = cardInfos;

    this.createCnt();
    this.createShadow();
    this.createCntCard();
    this.changeFrontCard(cardInfos);
    this.createBackCard();

    this.init();
  }


  // **************************** //
  // *****     CREATE      ****** //
  // **************************** //
  createCnt(){
    this.allCnt = new PIXI.Container();
    this.addChild(this.allCnt);
  }

  createCntCard(){
    this.cntCard = new PIXI.Container();
    this.allCnt.addChild(this.cntCard);
  }

  init(){
    this.magnetTopLeft = 0;
    this.magnetTopTop = 0;
    this.turnLeft = 0;
    this.isHidden = true;
    this.placement = "out";
  }

  // **************************** //
  // *****     SHADOW      ****** //
  // **************************** //
  createShadow(){
    this.shadow = new RectangleObject(W,H,0,5);
    this.allCnt.addChild(this.shadow);
    this.shadow.alpha = .15;
    this.shadow.filters = [ new PIXI.filters.BlurFilter(3)];
    this.shadow.pivot.x = W/2;
    this.shadow.pivot.y = H/2;
  }

  // ************************************** //
  // *****     CREATE BACK CARD      ****** //
  // ************************************** //
  createBackCard(){
    this.backCard = new PlaneObject(Asset.ATLAS, Asset.CARD_BACK, VECTOR_QUALITY,VECTOR_QUALITY);
    this.backCard.pivot.x = W/2;
    this.backCard.pivot.y = H/2;
    this.cntCard.addChild(this.backCard);
    this.savedVertices = [];
    for(let i=0;i<this.backCard.vertices.length;i++){
      this.savedVertices[i] = this.backCard.vertices[i];
    }

    this.maskCard = new RectangleObject(W,H*2,0,5);
    this.cntCard.addChild(this.maskCard);
    this.maskCard.pivot.x = W/2;
    this.maskCard.pivot.y = H/2;
    this.maskCard.y = -H/2;

    this.backCard.mask = this.maskCard;
  }

  // ******************************* //
  // *****     PLACEMENT      ****** //
  // ******************************* //
  putAs(placement,belongToOpponent,positionId){
  //  let tmpPlacement = this.placement;
    this.placement = placement;
    this.belongToOpponent = belongToOpponent;
    this.positionId = positionId;

    if(this.placement=="out"){
      this.setHidden(true);
      this.allCnt.x = 1200;
      this.allCnt.y = 200;
      this.setUp();
    }else if(this.placement=="deck"){
      let didAction = this.setHidden(true);
      TweenLite.to(this.allCnt, .5, {x:100+positionId*3 + (this.belongToOpponent?0:600), delay:didAction?.5:0, ease: Quad.easeOut});
      TweenLite.to(this.allCnt, .5, {y:360-positionId*2 , delay:didAction?.5:0, ease: Linear.easeNone});
      this.goDownAnim(.25);
    }else if(this.placement=="battle"){
      TweenLite.to(this.allCnt, .5, {x:(this.belongToOpponent?315:485), ease: Linear.easeNone});
      TweenLite.to(this.allCnt, .5, {y:240 , ease: Quad.easeOut});
      this.setHidden(false,.5);
      TweenLite.to(this.allCnt, .33, {x:(this.belongToOpponent?(400-43):(400+43)), delay:1.5, ease: Back.easeOut});
    }
  }

  setHidden(b,d=0){
    if(this.isHidden==b) return false;
    this.isHidden = b;
    if(this.isHidden){
      this.unrevealAnim(d);
    }else{
      this.revealAnim(d);
    }
    return true;
  }

  // ********************************* //
  // *****     UPDATE CARD      ****** //
  // ********************************* //
  changeFrontCard(cardInfos){
    if(this.frontCard){
      this.cntCard.removeChild(this.frontCard);
      this.frontCard = null;
    }
    this.frontCard = new PlaneObject(Asset.ATLAS,Asset.CARD_PLUS_ID_PNG+"0"+(cardInfos.isOpponent?"1":"0")+"_0"+cardInfos.num+".png",VECTOR_QUALITY,VECTOR_QUALITY);
    this.frontCard.pivot.x = W/2;
    this.frontCard.pivot.y = H/2;
    this.cntCard.addChild(this.frontCard);
  }


  // ******************************** //
  // *****     ANIM GO UP      ****** //
  // ******************************** //
  setUp(){
    this.shadow.x = -30;
    this.magnetTopTop = .5;
    this.magnetTopLeft = .5;
    this.cntCard.y = -100;
  }

  goUpAnim(d=0){
    TweenLite.to(this.shadow, .5, {x:-30, ease: Quad.easeIn, delay : d});
    TweenLite.to(this, .5, {magnetTopTop:0.5, ease: Quad.easeIn, delay : d});
    TweenLite.to(this, .5, {magnetTopLeft:0.5, ease: Quad.easeIn, delay : d});
    TweenLite.to(this.cntCard, .5, {y:-100, ease: Quad.easeIn, delay : d});
  }

  goDownAnim(d=0){
    TweenLite.to(this.shadow, .5, {x:0, ease: Quad.easeOut, delay : d});
    TweenLite.to(this, .5, {magnetTopTop:0, ease: Quad.easeOut, delay : d});
    TweenLite.to(this, .5, {magnetTopLeft:0, ease: Quad.easeOut, delay : d});
    TweenLite.to(this.cntCard, .5, {y:0, ease: Quad.easeOut, delay : d});
  }

  // ************************************** //
  // *****     ANIM UNREVEAL CARD      ****** //
  // ************************************** //
  unrevealAnim(d=0){
    TweenLite.to(this.maskCard, .33, {x:-W/2, ease: Quad.easeIn, delay : d});
    TweenLite.to(this.shadow.scale, .33, {x:0, ease: Quad.easeIn, delay : d});
    TweenLite.to(this, .18, {magnetTopTop:0.33, ease: Quad.easeIn, delay : d});
    TweenLite.to(this, .18, {magnetTopLeft:0.33, ease: Quad.easeIn, delay : d});
    TweenLite.to(this, .33, {turnLeft:1, ease: Quad.easeIn, delay : d, onComplete:()=>this.endUnrevealAnim()});
  }

  endUnrevealAnim(){
    TweenLite.to(this.maskCard, .33, {x:0, ease: Quad.easeOut});
    TweenLite.to(this.shadow.scale, .33, {x:1, ease: Quad.easeOut});
    TweenLite.to(this, .33, {magnetTopTop:0, ease: Quad.easeIn});
    TweenLite.to(this, .33, {magnetTopLeft:0, ease: Quad.easeOut});
    TweenLite.to(this, .33, {turnLeft:0, ease: Quad.easeOut});
  }

  // ************************************** //
  // *****     ANIM REVEAL CARD      ****** //
  // ************************************** //
  revealAnim(d=0){
    TweenLite.to(this.maskCard, .33, {x:-W/2, ease: Quad.easeIn, delay : d});
    TweenLite.to(this.shadow.scale, .33, {x:0, ease: Quad.easeIn, delay : d});
    TweenLite.to(this, .18, {magnetTopTop:0.33, ease: Quad.easeIn, delay : d});
    TweenLite.to(this, .18, {magnetTopLeft:0.33, ease: Quad.easeIn, delay : d});
    TweenLite.to(this, .33, {turnLeft:1, ease: Quad.easeIn, delay : d, onComplete:()=>this.endRevealAnim()});
  }

  endRevealAnim(){
    TweenLite.to(this.maskCard, .33, {x:-W, ease: Quad.easeOut});
    TweenLite.to(this.shadow.scale, .33, {x:1, ease: Quad.easeOut});
    TweenLite.to(this, .33, {magnetTopTop:0, ease: Quad.easeIn});
    TweenLite.to(this, .33, {magnetTopLeft:0, ease: Quad.easeOut});
    TweenLite.to(this, .33, {turnLeft:2, ease: Quad.easeOut});
  }

  // ************************************ //
  // *****     GET/SET/UPDATE      ****** //
  // ************************************ //
  get magnetTopTop(){
    return this.magnetTopTopValue;
  }

  set magnetTopTop(r){
    this.magnetTopTopValue = r;
    this.updateCard();
  }

  get turnLeft(){
    return this.turnLeftValue;
  }

  set turnLeft(r){
    this.turnLeftValue = r;
    this.updateCard();
  }

  get magnetTopLeft(){
    return this.magnetTopLeftValue;
  }

  set magnetTopLeft(r){
    this.magnetTopLeftValue = r;
    this.updateCard();
  }

  updateCard(){
    for(let i=0;i<this.savedVertices.length;i+=2){
      let xx = this.savedVertices[i];
      let yy = this.savedVertices[i+1];

      // ** plier le coin ** //
      let fx1 = (W-xx)*this.magnetTopLeftValue*(1-yy/H)*(1-yy/H);

      // ** tourner la carte ** //
      let fx2 = (W/2-xx)*this.turnLeftValue;
      let fy2 = -((H/2-yy)*(1-Math.abs(this.turnLeftValue-1))*(xx/W))/2;

      // **plier la carte encore plus ** //
      let fy3 = -(H-yy)*this.magnetTopTopValue*(1-xx/W)*(1-yy/H)*.25;

      let fx = xx+fx1+fx2;
      let fy = yy+fy2+fy3;

      this.backCard.vertices[i] = fx;
      this.frontCard.vertices[i] = fx;
      this.backCard.vertices[i+1] = fy;
      this.frontCard.vertices[i+1] = fy;
    }
  }

}
