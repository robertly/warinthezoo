import ParticuleObject from "./../plastique/object/ParticuleObject.js";
import SpriteObject from "./../plastique/object/SpriteObject.js";
import RectangleObject from "./../plastique/object/RectangleObject.js";
import CardObject from "./CardObject.js";
import Config from "./../Config.js";
import Command from "./../command/Command.js";
import GameState from "./../state/GameState.js";
import FontStyle from "./../style/FontStyle.js";
import Asset from "./../Asset.js";

const W = 130;
const H = 50;

export default class NextButton extends PIXI.Container{
    static get W(){ return W;}
    constructor() {
      super();

      this.cnt = new PIXI.Container();
      this.addChild(this.cnt);

      let bg = new RectangleObject(W,H,0xffffff,10);
      this.cnt.addChild(bg);
      this.cnt.pivot.x = W/2;
      this.cnt.pivot.y = H/2;

      let txt = new PIXI.Text("NEXT!");
      txt.x = 30;
      txt.y = 12;
      txt.style = FontStyle.STYLE_BTN;
      this.cnt.addChild(txt);

      this.colorMatrix = new PIXI.filters.ColorMatrixFilter();
      this.cnt.filters = [this.colorMatrix];

      this.setActive(false);

      let c = new Command();

      this.click = function (e) {
          c.emit(Command.ON_NEXT);
      }

      c.addListener(Command.IS_READY,()=>this.setActive(true));
      c.addListener(Command.WAIT_PLEASE,()=>this.setActive(false));

      this.mouseover = function (e) {
        if(this.isActive) TweenLite.to(this.cnt.scale, .5, {y:1.05,x:1.05, ease: Quad.easeOut});
      }

      this.mouseout = function (e) {
        if(this.isActive) TweenLite.to(this.cnt.scale, .5, {y:1,x:1, ease: Quad.easeOut});
      }

    }

    setActive(b){
      this.isActive = b
      this.interactive = b;
      this.buttonMode = b;
      this.colorMatrix.saturate(b?0:-1);
      this.cnt.alpha= (b?1:.5);
      TweenLite.to(this.cnt, .5, {y:(b?0:100), ease: Quad.easeOut});
    }

}
