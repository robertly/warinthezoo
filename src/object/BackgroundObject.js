import ParticuleObject from "./../plastique/object/ParticuleObject.js";
import SpriteObject from "./../plastique/object/SpriteObject.js";
import Config from "./../Config.js";
import FontStyle from "./../style/FontStyle.js";
import Asset from "./../Asset.js";


export default class BackgroundObject extends PIXI.Container{

  constructor() {
    super();
    this.createBg();
    this.createParticule();
    this.createAndStartAnimationText();
    this.createInstructions();
  }

  createBg(){
    let degrade = new SpriteObject(Asset.ATLAS, Asset.BG);
    this.addChild(degrade);
  }

  createParticule(){
    let particule = new ParticuleObject(Asset.ATLAS, Asset.STAR, Asset.PARTICULE_SNOW);
    this.addChild(particule);
    particule.startParticule();
  }


  createAndStartAnimationText(){
    let totalW=0;
    this.cntLetter = new PIXI.Container();
    this.addChild(this.cntLetter);

    let text = "war in the zoo!";
    let aText = text.split("");

    for(let i=0;i<aText.length;i++){
      let w = this.createOneLetter(aText[i],totalW,i);
      totalW+=w;
    }
    this.cntLetter.x = Config.STAGE.width/2 - totalW/2;
  }

  createOneLetter(letter,xx,d){
    if(letter == " ") return 12;
    let bitmapText = new PIXI.extras.BitmapText(letter, {font: "35px "+Asset.TEXT_BIGWIN, align: "left"});
    this.cntLetter.addChild(bitmapText);
    bitmapText.x = xx;
    bitmapText.y = -50;

    TweenLite.to(bitmapText, 1, {y:33, ease: Elastic.easeOut, delay : d*.1+.5});
    return bitmapText.textWidth;
  }

  createInstructions(){
      let txt = new PIXI.Text("Each player got cards from 1 to 5. The strongest card wins, except the 1 which beats the 5. When you win against a card, you take it in your deck. Take all the cards of your opponent to win!");
      txt.x = Config.STAGE.width/2 - FontStyle.STYLE_TXT.wordWrapWidth/2 ;
      txt.y = 100;
      txt.alpha = 0;
      txt.style = FontStyle.STYLE_TXT;
      this.addChild(txt);
      TweenLite.to(txt, 1, {y:80, alpha :1, ease: Quad.easeOut, delay : 2});
  }
}
