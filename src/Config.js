const DEBUG = false;
const STAGE = {width:800,height:450};

var app;

export default class Config {

  static get DEBUG(){ return DEBUG;}
  static get STAGE(){ return STAGE;}
  static set app(p){ app = p;}
  static get app(){ return app;}

  constructor() {
  }

}
