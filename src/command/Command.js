import EventEmitter from "./../plastique/event/EventEmitter.js";

let instance = null;
const ON_KEY_DOWN = 'ON_KEY_DOWN';
const ON_KEY_LEFT = 'ON_KEY_LEFT';
const ON_KEY_RIGHT = 'ON_KEY_RIGHT';
const ON_CARD = 'ON_CARD';
const ON_TURN = 'ON_TURN';
const ON_KEY_UP = 'ON_KEY_UP';
const ON_CLICK = 'ON_CLICK';
const ON_NEXT = 'ON_NEXT';
const ON_IMPACT = 'ON_IMPACT';
const IS_READY = 'IS_READY';
const PLEASE_WAIT = 'PLEASE_WAIT';
const ON_GAME_OVER = 'ON_GAME_OVER';
const ON_RELEASE_KEY_DOWN = 'ON_RELEASE_KEY_DOWN';

export default class Command extends EventEmitter{

  static get ON_CARD(){ return ON_CARD;}
  static get ON_TURN(){ return ON_TURN;}
  static get ON_KEY_DOWN(){ return ON_KEY_DOWN;}
  static get ON_KEY_LEFT(){ return ON_KEY_LEFT;}
  static get ON_KEY_RIGHT(){ return ON_KEY_RIGHT;}
  static get ON_KEY_UP(){ return ON_KEY_UP;}
  static get ON_GAME_OVER(){ return ON_GAME_OVER;}
  static get ON_CLICK(){ return ON_CLICK;}
  static get ON_IMPACT(){ return ON_IMPACT;}
  static get IS_READY(){ return IS_READY;}
  static get PLEASE_WAIT(){ return PLEASE_WAIT;}
  static get ON_NEXT(){ return ON_NEXT;}
  static get ON_RELEASE_KEY_DOWN(){ return ON_RELEASE_KEY_DOWN;}

  constructor() {
    super();
    if(!instance){instance = this;}
    return instance;
  }
}
