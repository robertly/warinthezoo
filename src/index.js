import Asset from "./Asset.js";
import Config from "./Config.js";
import Main from "./Main.js";
import Command from "./command/Command.js";
import GameState from "./state/GameState.js";
import FontStyle from "./style/FontStyle.js";
import SoundManager from "./sound/SoundManager.js";
import RectangleObject from "./plastique/object/RectangleObject.js";

var app;
var bg;
var cnt;
var loadingTxt;

initApp();

function initApp(){
    PIXI.utils.sayHello(PIXI.utils.isWebGLSupported()?"WebGL":"canvas");
    app = new PIXI.Application(Config.STAGE.width, Config.STAGE.height, {backgroundColor : 0x1099bb, antialias: true });
    app.view.x = getWindowSize().width/2-Config.STAGE.width/2;
    app.view.x = getWindowSize().height/2-Config.STAGE.height/2;
    Config.app = app;
    document.getElementById("pixiDiv").appendChild(app.view);
    document.getElementById("pixiDiv").style.marginLeft = -Config.STAGE.width/2+"px";
    document.getElementById("pixiDiv").style.marginTop = -Config.STAGE.height/2+"px";
    document.body.onclick=onClick;
    document.body.onkeydown=onKeyDown;
    document.body.onkeyup=onKeyUp;
    PIXI.loader
      .add(Asset.ATLAS)
      .add(Asset.PARTICULE_SNOW)
      .add(Asset.PARTICULE_STAR)
      .add(Asset.TEXT_BIGWIN_XML)
      .on("progress", loadProgressHandler)
      .load(setup);
    cnt = new PIXI.Container();
    bg = new RectangleObject(Config.STAGE.width,Config.STAGE.height, 0x1099bb,0);
    loadingTxt = new PIXI.Text("LOADING");
    loadingTxt.x = 345;
    loadingTxt.y = 220;
    loadingTxt.style = FontStyle.STYLE_LOADING;
    app.stage.addChild(cnt);
    app.stage.addChild(bg);
    app.stage.addChild(loadingTxt);
}

function getWindowSize(){
    let w = window,d = document,e = d.documentElement, g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight|| e.clientHeight|| g.clientHeight;
    return {width:x,height:y};
}

function loadProgressHandler(loader, resource) {
  console.log("loading: " + resource.url);
  console.log("progress: " + loader.progress + "%");
}

function onKeyUp(e){
    let command = new Command();
    if(e.keyCode == 40) {
        command.emit(Command.ON_RELEASE_KEY_DOWN);
    }
}

function onKeyDown(e){
    let command = new Command();
    if(e.keyCode == 37) {
        command.emit(Command.ON_KEY_LEFT);
    }else if(e.keyCode == 38) {
        command.emit(Command.ON_KEY_UP);
    }else if(e.keyCode == 39) {
        command.emit(Command.ON_KEY_RIGHT);
    }else if(e.keyCode == 40) {
        command.emit(Command.ON_KEY_DOWN);
    }
}

function onClick(){
    let command = new Command();
    command.emit(Command.ON_CLICK);
}

function setup(){
    let sm = new SoundManager();
    sm.addListener(SoundManager.IS_LOADED, ()=>startApp());
    sm.load();
}

function startApp(){
    console.log("START APP!!!");
    let mainStage = new Main(app);
    cnt.addChild(mainStage);
    TweenLite.to(bg, .25, {alpha:0, delay:.25,ease: Quad.easeIn});
    TweenLite.to(loadingTxt, .25, {alpha:0, ease: Quad.easeIn});
}
