export default class SpriteObject2d extends PIXI.projection.Sprite2d{

  constructor(fileNameAtlas,fileNameImg) {
    super(PIXI.loader.resources[fileNameAtlas].textures[fileNameImg]);
  }

}
