export default class CircleObject extends PIXI.Graphics{

  constructor(radius,color) {
    super();
    this.beginFill(color);
    this.drawCircle(0, 0, radius);
    this.endFill();
  }

}
