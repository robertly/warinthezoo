export default class RectangleObject extends PIXI.Graphics{
        
  constructor(w,h,color,round=0) {
    super();
    this.beginFill(color);
    this.drawRoundedRect(0, 0, w, h,round);
    this.endFill();
  }
    
}
