import SpriteObject from "./SpriteObject.js";


export default class ParticuleObject extends PIXI.Container{

  constructor(atlasFilename,textureFilename,particuleFilename,duration=-1) {
    super();
    let texture = (new SpriteObject(atlasFilename,textureFilename)).texture;
    let jsonParticule = PIXI.loader.resources[particuleFilename].data;
    this.emitter = new PIXI.particles.Emitter(
                    this,
                    [texture],
                    jsonParticule
                    );
    this.emitter.emit = true;
    this.duration = duration;
    this.maxTime = jsonParticule.lifetime.max;
  }

  startParticule(){
    this.isOver=false;
    this.elapsed = Date.now();
    this.comptParticule = 0;
    this.updateParticule();
  }

  stopParticule(){
    this.comptParticule = 0;
    this.isOver=true;
    this.emitter.emit = false;
  }

  updateParticule(){
    let now = Date.now();
    let d = (now - this.elapsed)*0.001;
    this.comptParticule+=d;
    if(this.isOver){
      if(this.comptParticule>(this.maxTime)) return;
    }else if(this.duration!=-1){
      if(this.comptParticule>=this.duration) this.emitter.emit = false;
      if(this.comptParticule>(this.maxTime+this.duration)) return;
    }
    requestAnimationFrame(()=>this.updateParticule());
    this.emitter.update(d);
    this.elapsed = now;
  }
}
