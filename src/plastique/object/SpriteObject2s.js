export default class SpriteObject2s extends PIXI.projection.Sprite2s{

  constructor(fileNameAtlas,fileNameImg) {
    super(PIXI.loader.resources[fileNameAtlas].textures[fileNameImg]);
  }

}
