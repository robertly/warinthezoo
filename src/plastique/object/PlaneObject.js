export default class PlaneObject extends PIXI.mesh.Plane{

  constructor(fileNameAtlas,fileNameImg,xx,yy) {
    super(PIXI.loader.resources[fileNameAtlas].textures[fileNameImg],xx,yy);
  }

}
