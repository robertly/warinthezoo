export default class SpriteObject extends PIXI.Sprite{

  constructor(fileNameAtlas,fileNameImg) {
    super(PIXI.loader.resources[fileNameAtlas].textures[fileNameImg]);
  }

}
