import BackgroundObject from "./object/BackgroundObject.js";
import GameWorldObject from "./object/GameWorldObject.js";
import NextButton from "./object/NextButton.js";
import GameState from "./state/GameState.js";
import SoundManager from "./sound/SoundManager.js";
import Config from "./Config.js";

export default class Main extends PIXI.Container{

    constructor(app) {
        super();
        this.startGame();

        app.ticker.add((delta)=>this.onInterval(delta));

    }


    // ********************** //
    // *****   START   ****** //
    // ********************** //
    startGame(){
        this.initGame();
    }

    initGame(){
        this.nbTick = 0;

        let game = new GameState();

        let background = new BackgroundObject();
        this.addChild(background);

        let gameWorld = new GameWorldObject();
        this.addChild(gameWorld);

        let nextButton = new NextButton();
        this.addChild(nextButton);
        nextButton.x = Config.STAGE.width/2;
        nextButton.y = 375;
    }

    // ************************* //
    // *****   INTERVAL   ****** //
    // ************************* //

    // ************************* //
    // *****   INTERVAL   ****** //
    // ************************* //
    onInterval(delta){
      this.nbTick+=1+delta;
    }
}
