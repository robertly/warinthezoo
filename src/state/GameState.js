import EventEmitter from "./../plastique/event/EventEmitter.js";

let instance = null;

export default class GameState extends EventEmitter{

  constructor() {
    super();
    if(!instance){
      instance = this;
      this.createNewGame();
    }
    return instance;
  }

  createNewGame(){
    let b = true;
    let copyDeckOpponent, copyDeckPlayer;
    while(b){
      this.deckOpponent = this.getRandomCard(true);
      this.deckPlayer = this.getRandomCard(false);
      copyDeckOpponent = this.getCopyDeck(this.deckOpponent);
      copyDeckPlayer = this.getCopyDeck(this.deckPlayer);

      let res = this.simulateResult();
      if(res.nbTurn<25) b=false;
    }

    this.deckOpponent = copyDeckOpponent;
    this.deckPlayer = copyDeckPlayer;
  }

  getCopyDeck(a){
    let aOut = [];
    for(let i=0;i<a.length;i++){
      aOut.push({num:a[i].num,isOpponent:a[i].isOpponent});
    }
    return aOut;
  }

  simulateResult(){
    for(let i=0;i<50;i++){
      if(instance.isFinito()){
        return {nbTurn:i, winner:instance.doYouWin()?"you win":"you lose"};
      }

      let card1 = instance.removeCardOpponent();
      let card2 = instance.removeCardPlayer();
      if(card1.num == card2.num){
        instance.addToOpponentDeck(card1);
        instance.addToPlayerDeck(card2);
      } else if(card1.num == 5 && card2.num == 1){
        instance.addToPlayerDeck(card2);
        instance.addToPlayerDeck(card1);
      } else if(card1.num == 1 && card2.num == 5){
        instance.addToOpponentDeck(card2);
        instance.addToOpponentDeck(card1);
      } else if(card1.num > card2.num){
        instance.addToOpponentDeck(card2);
        instance.addToOpponentDeck(card1);
      } else{
        instance.addToPlayerDeck(card2);
        instance.addToPlayerDeck(card1);
      }
    }

    return {nbTurn:50, winner:"too long"};

  }

  // ************************* //
  // ****     WINNING     **** //
  // ************************* //
  doYouWin(){
    if(instance.deckOpponent.length<=0) return true;
    return false;
  }

  isFinito(){
    if(instance.deckPlayer.length<=0) return true;
    if(instance.deckOpponent.length<=0) return true;
    return false;
  }


  // ******************************** //
  // ****     GET NEXT CARDS     **** //
  // ******************************** //
  getNextCardOpponent(){
    return instance.deckOpponent[instance.deckOpponent.length-1];
  }

  getNextCardPlayer(){
    return instance.deckPlayer[instance.deckPlayer.length-1];
  }

  removeCardPlayer(){
    let card = instance.getNextCardPlayer();
    let index = instance.deckPlayer.indexOf(card);
    instance.deckPlayer.splice(index, 1);
    return card;
  }

  removeCardOpponent(){
    let card = instance.getNextCardOpponent();
    let index = instance.deckOpponent.indexOf(card);
    instance.deckOpponent.splice(index, 1);
    return card;
  }

  // ***************************** //
  // ****     ADD TO DECK     **** //
  // ***************************** //
  addToPlayerDeck(card){
    instance.deckPlayer = [card].concat(instance.deckPlayer);
  }

  addToOpponentDeck(card){
    instance.deckOpponent = [card].concat(instance.deckOpponent);
  }

  // *********************** //
  // ****     TRACE     **** //
  // *********************** //
  getDeckPlayerInfos(){
    let s= "";
    for(let i=0;i<instance.deckPlayer.length;i++){
      if(instance.deckPlayer[i].isOpponent) s+="B"+instance.deckPlayer[i].num+" / ";
      else s+="A"+instance.deckPlayer[i].num+" / ";
    }
    return s;
  }

  getDeckOpponentInfos(){
    let s= "";
    for(let i=0;i<instance.deckOpponent.length;i++){
      if(instance.deckOpponent[i].isOpponent) s+="B"+instance.deckOpponent[i].num+" / ";
      else s+="A"+instance.deckOpponent[i].num+" / ";
    }
    return s;
  }

  // ************************* //
  // ****     SHUFFLE     **** //
  // ************************* //
  getRandomCard(isOpponent){
  /*  if(isOpponent) return [{num:4,isOpponent:true},{num:5,isOpponent:true},{num:1,isOpponent:true},{num:2,isOpponent:true},{num:3,isOpponent:true}];
    else return [{num:5,isOpponent:false},{num:1,isOpponent:false},{num:2,isOpponent:false},{num:3,isOpponent:false},{num:4,isOpponent:false}];
*/

    let cardA = [1,2,3,4,5];
    let a=[];
    while(cardA.length>0){
      let r= Math.floor(Math.random()*cardA.length);
      let out = cardA[r];
      a.push({num:out,isOpponent:isOpponent});
      let index = cardA.indexOf(out);
      cardA.splice(index, 1);
    }
    return a;
  }
}
