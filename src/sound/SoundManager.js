import Command from "./../command/Command.js";
import Asset from "./../Asset.js";
import EventEmitter from "./../plastique/event/EventEmitter.js";

let instance = null;

const IS_LOADED = "IS_LOADED";

const TIME_DOIING = 0;
const TIME_IMPACT = 540;
const TIME_TADAA = 800;
const TIME_WHIP_BACK = 2460;
const TIME_WHIP_GO = 2760;
const TIME_END = 3120;

export default class SoundManager extends EventEmitter{

    static get IS_LOADED(){ return IS_LOADED;}

    constructor() {
      super();
      if(!instance){
        instance = this;
        instance.aSoundToLoad = [{src:[Asset.SOUND_ATLAS],
                                  sprite:{
                                    doiing:[TIME_DOIING,TIME_IMPACT-TIME_DOIING-1],
                                    impact:[TIME_IMPACT,TIME_TADAA-TIME_IMPACT-1],
                                    tadaa:[TIME_TADAA,TIME_WHIP_BACK-TIME_TADAA-1],
                                    whip_back:[TIME_WHIP_BACK,TIME_WHIP_GO-TIME_WHIP_BACK-1],
                                    whip_go:[TIME_WHIP_GO,TIME_END-TIME_WHIP_GO]
                                  }
                                }];
      }
      return instance;
    }

    load(){
      instance.loadNextAsset();

      var command = new Command();
      command.addListener(Command.ON_CARD, ()=>instance.playSound(Asset.SOUND_ATLAS,"whip_back"));
      command.addListener(Command.ON_TURN, ()=>instance.playSound(Asset.SOUND_ATLAS,"whip_go"));
      command.addListener(Command.ON_GAME_OVER, ()=>instance.playSound(Asset.SOUND_ATLAS,"tadaa"));
      command.addListener(Command.ON_NEXT, ()=>instance.playSound(Asset.SOUND_ATLAS,"impact"));
      command.addListener(Command.ON_IMPACT, ()=>instance.playSound(Asset.SOUND_ATLAS,"doiing"));
    }

    loadNextAsset(){
      if(instance.aSoundToLoad.length == 0){
        console.log("finitoLoadingSound!");
        instance.emit(IS_LOADED);
        return;
      }
      let asset = instance.aSoundToLoad.pop();
      instance.loadAsset(asset);
    }

    loadAsset(asset){
      console.log("loadingSound: "+asset);
      let sound = new Howl(asset);
      if(!instance.aSound) instance.aSound=[];
      instance.aSound.push({sound:sound, asset:asset.src[0]});
      sound.once('load', ()=>instance.finishLoadAsset());
    }

    playSound(asset,timing){
      (instance.getMeSound(asset)).play(timing);
    }

    getMeSound(asset){
      for(let i=0;i<instance.aSound.length;i++){
        if(instance.aSound[i].asset == asset) return instance.aSound[i].sound;
      }
    }

    finishLoadAsset(){
      instance.loadNextAsset();
    }

}
