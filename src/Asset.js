const CARD_PLUS_ID_PNG = "card_";
const CROWN = "crown.png";
const STAR = "star.png";
const CARD_BACK = "card_back.png";
const BG = "bg.jpg";

const ATLAS = "asset/asset.json";

const PARTICULE_STAR = "asset/particule/particule_star.json";
const PARTICULE_SNOW = "asset/particule/particule_snow.json";

const SOUND_WHIP_GO = "asset/sound/whip_go.mp3";
const SOUND_WHIP_BACK = "asset/sound/whip_back.mp3";
const SOUND_BTN = "asset/sound/btn.mp3";
const SOUND_DOIING = "asset/sound/doiing.mp3";
const SOUND_TADAA = "asset/sound/tadaa.mp3";
const SOUND_TOUCH = "asset/sound/touch.mp3";
const SOUND_IMPACT = "asset/sound/impact.mp3";
const SOUND_ATLAS = "asset/sound/atlas_sound.mp3";

const TEXT_BIGWIN_XML = "asset/bigwin-export.xml";
const TEXT_BIGWIN = "bigwin-export";


export default class Asset {

  static get ATLAS(){ return ATLAS;}

  static get PARTICULE_SNOW(){ return PARTICULE_SNOW;}
  static get PARTICULE_STAR(){ return PARTICULE_STAR;}

  static get SOUND_ATLAS(){ return SOUND_ATLAS;}
  static get SOUND_WHIP_BACK(){ return SOUND_WHIP_BACK;}
  static get SOUND_WHIP_GO(){ return SOUND_WHIP_GO;}
  static get SOUND_IMPACT(){ return SOUND_IMPACT;}
  static get SOUND_BTN(){ return SOUND_BTN;}
  static get SOUND_DOIING(){ return SOUND_DOIING;}
  static get SOUND_TADAA(){ return SOUND_TADAA;}
  static get SOUND_TOUCH(){ return SOUND_TOUCH;}

  static get TEXT_BIGWIN_XML(){ return TEXT_BIGWIN_XML;}
  static get TEXT_BIGWIN(){ return TEXT_BIGWIN;}

  static get CARD_PLUS_ID_PNG(){ return CARD_PLUS_ID_PNG;}
  static get CROWN(){ return CROWN;}
  static get STAR(){ return STAR;}
  static get CARD_BACK(){ return CARD_BACK;}
  static get BG(){ return BG;}


  constructor() {
  }

}
