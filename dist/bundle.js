/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 11);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
const CARD_PLUS_ID_PNG = "card_";
const CROWN = "crown.png";
const STAR = "star.png";
const CARD_BACK = "card_back.png";
const BG = "bg.jpg";

const ATLAS = "asset/asset.json";

const PARTICULE_STAR = "asset/particule/particule_star.json";
const PARTICULE_SNOW = "asset/particule/particule_snow.json";

const SOUND_WHIP_GO = "asset/sound/whip_go.mp3";
const SOUND_WHIP_BACK = "asset/sound/whip_back.mp3";
const SOUND_BTN = "asset/sound/btn.mp3";
const SOUND_DOIING = "asset/sound/doiing.mp3";
const SOUND_TADAA = "asset/sound/tadaa.mp3";
const SOUND_TOUCH = "asset/sound/touch.mp3";
const SOUND_IMPACT = "asset/sound/impact.mp3";
const SOUND_ATLAS = "asset/sound/atlas_sound.mp3";

const TEXT_BIGWIN_XML = "asset/bigwin-export.xml";
const TEXT_BIGWIN = "bigwin-export";


class Asset {

  static get ATLAS(){ return ATLAS;}

  static get PARTICULE_SNOW(){ return PARTICULE_SNOW;}
  static get PARTICULE_STAR(){ return PARTICULE_STAR;}

  static get SOUND_ATLAS(){ return SOUND_ATLAS;}
  static get SOUND_WHIP_BACK(){ return SOUND_WHIP_BACK;}
  static get SOUND_WHIP_GO(){ return SOUND_WHIP_GO;}
  static get SOUND_IMPACT(){ return SOUND_IMPACT;}
  static get SOUND_BTN(){ return SOUND_BTN;}
  static get SOUND_DOIING(){ return SOUND_DOIING;}
  static get SOUND_TADAA(){ return SOUND_TADAA;}
  static get SOUND_TOUCH(){ return SOUND_TOUCH;}

  static get TEXT_BIGWIN_XML(){ return TEXT_BIGWIN_XML;}
  static get TEXT_BIGWIN(){ return TEXT_BIGWIN;}

  static get CARD_PLUS_ID_PNG(){ return CARD_PLUS_ID_PNG;}
  static get CROWN(){ return CROWN;}
  static get STAR(){ return STAR;}
  static get CARD_BACK(){ return CARD_BACK;}
  static get BG(){ return BG;}


  constructor() {
  }

}
/* harmony export (immutable) */ __webpack_exports__["a"] = Asset;



/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
const DEBUG = false;
const STAGE = {width:800,height:450};

var app;

class Config {

  static get DEBUG(){ return DEBUG;}
  static get STAGE(){ return STAGE;}
  static set app(p){ app = p;}
  static get app(){ return app;}

  constructor() {
  }

}
/* harmony export (immutable) */ __webpack_exports__["a"] = Config;



/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class SpriteObject extends PIXI.Sprite{

  constructor(fileNameAtlas,fileNameImg) {
    super(PIXI.loader.resources[fileNameAtlas].textures[fileNameImg]);
  }

}
/* harmony export (immutable) */ __webpack_exports__["a"] = SpriteObject;



/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
const STYLE_SCORING = new PIXI.TextStyle({fontFamily: 'Franklin Gothic',fontSize: 50, stroke:0x085163,strokeThickness:10, align:"center", wordWrapWidth:600, fill: "white",letterSpacing:1});
const STYLE_GAME_OVER = new PIXI.TextStyle({fontFamily: 'Franklin Gothic',fontSize: 70, stroke:0x085163,strokeThickness:0, align:"center", wordWrapWidth:600, fill: "white",letterSpacing:1});
const STYLE_GAME_OVER_SCORE = new PIXI.TextStyle({fontFamily: 'Arial',fontSize: 30, stroke:0x085163,strokeThickness:0, align:"center", wordWrapWidth:600, fill: "white",letterSpacing:1});
const STYLE_LEVELING = new PIXI.TextStyle({fontFamily: 'Franklin Gothic',fontSize: 50, stroke:"white",strokeThickness:10, align:"center", wordWrapWidth:600, fill:0x085163,letterSpacing:1});
const STYLE_TITLE = new PIXI.TextStyle({fontFamily: 'Franklin Gothic',fontSize: 36,fill: "white",letterSpacing:1});
const STYLE_SUBTITLE = new PIXI.TextStyle({fontFamily: 'Franklin Gothic',fontSize: 22,fill: "white",letterSpacing:1});
const STYLE_TXT = new PIXI.TextStyle({fontFamily: 'Arial',fontSize: 12,fill: "white",letterSpacing:.5,lineHeight:12, wordWrapWidth:300, align:"center",  wordWrap:true});
const STYLE_BTN = new PIXI.TextStyle({fontFamily: 'Franklin Gothic',fontSize: 22,fill: 0x1099bb,letterSpacing:2,lineHeight:12, wordWrapWidth:100, align:"center",  wordWrap:true});
const STYLE_NEXT = new PIXI.TextStyle({fontFamily: 'Franklin Gothic',fontSize: 16,fill: 0x1099bb,letterSpacing:1});
const STYLE_LOADING = new PIXI.TextStyle({fontFamily: 'Franklin Gothic',fontSize: 22,fill: 0x085163,letterSpacing:1});

class FontStyle {
    static get STYLE_TITLE(){ return STYLE_TITLE;}
    static get STYLE_SUBTITLE(){ return STYLE_SUBTITLE;}
    static get STYLE_BTN(){ return STYLE_BTN;}
    static get STYLE_TXT(){ return STYLE_TXT;}
    static get STYLE_NEXT(){ return STYLE_NEXT;}
    static get STYLE_SCORING(){ return STYLE_SCORING;}
    static get STYLE_LEVELING(){ return STYLE_LEVELING;}
    static get STYLE_LOADING(){ return STYLE_LOADING;}
    static get STYLE_GAME_OVER(){ return STYLE_GAME_OVER;}
    static get STYLE_GAME_OVER_SCORE(){ return STYLE_GAME_OVER_SCORE;}
}
/* harmony export (immutable) */ __webpack_exports__["a"] = FontStyle;



/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__SpriteObject_js__ = __webpack_require__(2);



class ParticuleObject extends PIXI.Container{

  constructor(atlasFilename,textureFilename,particuleFilename,duration=-1) {
    super();
    let texture = (new __WEBPACK_IMPORTED_MODULE_0__SpriteObject_js__["a" /* default */](atlasFilename,textureFilename)).texture;
    let jsonParticule = PIXI.loader.resources[particuleFilename].data;
    this.emitter = new PIXI.particles.Emitter(
                    this,
                    [texture],
                    jsonParticule
                    );
    this.emitter.emit = true;
    this.duration = duration;
    this.maxTime = jsonParticule.lifetime.max;
  }

  startParticule(){
    this.isOver=false;
    this.elapsed = Date.now();
    this.comptParticule = 0;
    this.updateParticule();
  }

  stopParticule(){
    this.comptParticule = 0;
    this.isOver=true;
    this.emitter.emit = false;
  }

  updateParticule(){
    let now = Date.now();
    let d = (now - this.elapsed)*0.001;
    this.comptParticule+=d;
    if(this.isOver){
      if(this.comptParticule>(this.maxTime)) return;
    }else if(this.duration!=-1){
      if(this.comptParticule>=this.duration) this.emitter.emit = false;
      if(this.comptParticule>(this.maxTime+this.duration)) return;
    }
    requestAnimationFrame(()=>this.updateParticule());
    this.emitter.update(d);
    this.elapsed = now;
  }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = ParticuleObject;



/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class RectangleObject extends PIXI.Graphics{
        
  constructor(w,h,color,round=0) {
    super();
    this.beginFill(color);
    this.drawRoundedRect(0, 0, w, h,round);
    this.endFill();
  }
    
}
/* harmony export (immutable) */ __webpack_exports__["a"] = RectangleObject;



/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__plastique_event_EventEmitter_js__ = __webpack_require__(8);


let instance = null;
const ON_KEY_DOWN = 'ON_KEY_DOWN';
const ON_KEY_LEFT = 'ON_KEY_LEFT';
const ON_KEY_RIGHT = 'ON_KEY_RIGHT';
const ON_CARD = 'ON_CARD';
const ON_TURN = 'ON_TURN';
const ON_KEY_UP = 'ON_KEY_UP';
const ON_CLICK = 'ON_CLICK';
const ON_NEXT = 'ON_NEXT';
const ON_IMPACT = 'ON_IMPACT';
const IS_READY = 'IS_READY';
const PLEASE_WAIT = 'PLEASE_WAIT';
const ON_GAME_OVER = 'ON_GAME_OVER';
const ON_RELEASE_KEY_DOWN = 'ON_RELEASE_KEY_DOWN';

class Command extends __WEBPACK_IMPORTED_MODULE_0__plastique_event_EventEmitter_js__["a" /* default */]{

  static get ON_CARD(){ return ON_CARD;}
  static get ON_TURN(){ return ON_TURN;}
  static get ON_KEY_DOWN(){ return ON_KEY_DOWN;}
  static get ON_KEY_LEFT(){ return ON_KEY_LEFT;}
  static get ON_KEY_RIGHT(){ return ON_KEY_RIGHT;}
  static get ON_KEY_UP(){ return ON_KEY_UP;}
  static get ON_GAME_OVER(){ return ON_GAME_OVER;}
  static get ON_CLICK(){ return ON_CLICK;}
  static get ON_IMPACT(){ return ON_IMPACT;}
  static get IS_READY(){ return IS_READY;}
  static get PLEASE_WAIT(){ return PLEASE_WAIT;}
  static get ON_NEXT(){ return ON_NEXT;}
  static get ON_RELEASE_KEY_DOWN(){ return ON_RELEASE_KEY_DOWN;}

  constructor() {
    super();
    if(!instance){instance = this;}
    return instance;
  }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Command;



/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__plastique_event_EventEmitter_js__ = __webpack_require__(8);


let instance = null;

class GameState extends __WEBPACK_IMPORTED_MODULE_0__plastique_event_EventEmitter_js__["a" /* default */]{

  constructor() {
    super();
    if(!instance){
      instance = this;
      this.createNewGame();
    }
    return instance;
  }

  createNewGame(){
    let b = true;
    let copyDeckOpponent, copyDeckPlayer;
    while(b){
      this.deckOpponent = this.getRandomCard(true);
      this.deckPlayer = this.getRandomCard(false);
      copyDeckOpponent = this.getCopyDeck(this.deckOpponent);
      copyDeckPlayer = this.getCopyDeck(this.deckPlayer);

      let res = this.simulateResult();
      if(res.nbTurn<25) b=false;
    }

    this.deckOpponent = copyDeckOpponent;
    this.deckPlayer = copyDeckPlayer;
  }

  getCopyDeck(a){
    let aOut = [];
    for(let i=0;i<a.length;i++){
      aOut.push({num:a[i].num,isOpponent:a[i].isOpponent});
    }
    return aOut;
  }

  simulateResult(){
    for(let i=0;i<50;i++){
      if(instance.isFinito()){
        return {nbTurn:i, winner:instance.doYouWin()?"you win":"you lose"};
      }

      let card1 = instance.removeCardOpponent();
      let card2 = instance.removeCardPlayer();
      if(card1.num == card2.num){
        instance.addToOpponentDeck(card1);
        instance.addToPlayerDeck(card2);
      } else if(card1.num == 5 && card2.num == 1){
        instance.addToPlayerDeck(card2);
        instance.addToPlayerDeck(card1);
      } else if(card1.num == 1 && card2.num == 5){
        instance.addToOpponentDeck(card2);
        instance.addToOpponentDeck(card1);
      } else if(card1.num > card2.num){
        instance.addToOpponentDeck(card2);
        instance.addToOpponentDeck(card1);
      } else{
        instance.addToPlayerDeck(card2);
        instance.addToPlayerDeck(card1);
      }
    }

    return {nbTurn:50, winner:"too long"};

  }

  // ************************* //
  // ****     WINNING     **** //
  // ************************* //
  doYouWin(){
    if(instance.deckOpponent.length<=0) return true;
    return false;
  }

  isFinito(){
    if(instance.deckPlayer.length<=0) return true;
    if(instance.deckOpponent.length<=0) return true;
    return false;
  }


  // ******************************** //
  // ****     GET NEXT CARDS     **** //
  // ******************************** //
  getNextCardOpponent(){
    return instance.deckOpponent[instance.deckOpponent.length-1];
  }

  getNextCardPlayer(){
    return instance.deckPlayer[instance.deckPlayer.length-1];
  }

  removeCardPlayer(){
    let card = instance.getNextCardPlayer();
    let index = instance.deckPlayer.indexOf(card);
    instance.deckPlayer.splice(index, 1);
    return card;
  }

  removeCardOpponent(){
    let card = instance.getNextCardOpponent();
    let index = instance.deckOpponent.indexOf(card);
    instance.deckOpponent.splice(index, 1);
    return card;
  }

  // ***************************** //
  // ****     ADD TO DECK     **** //
  // ***************************** //
  addToPlayerDeck(card){
    instance.deckPlayer = [card].concat(instance.deckPlayer);
  }

  addToOpponentDeck(card){
    instance.deckOpponent = [card].concat(instance.deckOpponent);
  }

  // *********************** //
  // ****     TRACE     **** //
  // *********************** //
  getDeckPlayerInfos(){
    let s= "";
    for(let i=0;i<instance.deckPlayer.length;i++){
      if(instance.deckPlayer[i].isOpponent) s+="B"+instance.deckPlayer[i].num+" / ";
      else s+="A"+instance.deckPlayer[i].num+" / ";
    }
    return s;
  }

  getDeckOpponentInfos(){
    let s= "";
    for(let i=0;i<instance.deckOpponent.length;i++){
      if(instance.deckOpponent[i].isOpponent) s+="B"+instance.deckOpponent[i].num+" / ";
      else s+="A"+instance.deckOpponent[i].num+" / ";
    }
    return s;
  }

  // ************************* //
  // ****     SHUFFLE     **** //
  // ************************* //
  getRandomCard(isOpponent){
  /*  if(isOpponent) return [{num:4,isOpponent:true},{num:5,isOpponent:true},{num:1,isOpponent:true},{num:2,isOpponent:true},{num:3,isOpponent:true}];
    else return [{num:5,isOpponent:false},{num:1,isOpponent:false},{num:2,isOpponent:false},{num:3,isOpponent:false},{num:4,isOpponent:false}];
*/

    let cardA = [1,2,3,4,5];
    let a=[];
    while(cardA.length>0){
      let r= Math.floor(Math.random()*cardA.length);
      let out = cardA[r];
      a.push({num:out,isOpponent:isOpponent});
      let index = cardA.indexOf(out);
      cardA.splice(index, 1);
    }
    return a;
  }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = GameState;



/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
let isFunction = function(obj) {  
    return typeof obj == 'function' || false;
};

class EventEmitter {  
    constructor() {
        this.listeners = new Map();
    }
    
  // *********************** //
  // *****   LISTEN   ****** //
  // *********************** //
    addListener(label, callback) {
        this.listeners.has(label) || this.listeners.set(label, []);
        this.listeners.get(label).push(callback);
    }

    removeListener(label, callback) {  
        let listeners = this.listeners.get(label),
            index;

        if (listeners && listeners.length) {
            index = listeners.reduce((i, listener, index) => {
                return (isFunction(listener) && listener === callback) ?
                    i = index :
                    i;
            }, -1);

            if (index > -1) {
                listeners.splice(index, 1);
                this.listeners.set(label, listeners);
                return true;
            }
        }
        return false;
    }
    
  // ********************* //
  // *****   EMIT   ****** //
  // ********************* //
    emit(label, ...args) {  
        let listeners = this.listeners.get(label);

        if (listeners && listeners.length) {
            listeners.forEach((listener) => {
                listener(...args); 
            });
            return true;
        }
        return false;
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = EventEmitter;


/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__plastique_object_ParticuleObject_js__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__plastique_object_PlaneObject_js__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__plastique_object_SpriteObject_js__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__plastique_object_RectangleObject_js__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Config_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__style_FontStyle_js__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__Asset_js__ = __webpack_require__(0);









const VECTOR_QUALITY = 5;
const W = 78;
const H = 117;

class CardObject extends PIXI.Container{

  constructor(cardInfos) {
    super();
    this.infos = cardInfos;

    this.createCnt();
    this.createShadow();
    this.createCntCard();
    this.changeFrontCard(cardInfos);
    this.createBackCard();

    this.init();
  }


  // **************************** //
  // *****     CREATE      ****** //
  // **************************** //
  createCnt(){
    this.allCnt = new PIXI.Container();
    this.addChild(this.allCnt);
  }

  createCntCard(){
    this.cntCard = new PIXI.Container();
    this.allCnt.addChild(this.cntCard);
  }

  init(){
    this.magnetTopLeft = 0;
    this.magnetTopTop = 0;
    this.turnLeft = 0;
    this.isHidden = true;
    this.placement = "out";
  }

  // **************************** //
  // *****     SHADOW      ****** //
  // **************************** //
  createShadow(){
    this.shadow = new __WEBPACK_IMPORTED_MODULE_3__plastique_object_RectangleObject_js__["a" /* default */](W,H,0,5);
    this.allCnt.addChild(this.shadow);
    this.shadow.alpha = .15;
    this.shadow.filters = [ new PIXI.filters.BlurFilter(3)];
    this.shadow.pivot.x = W/2;
    this.shadow.pivot.y = H/2;
  }

  // ************************************** //
  // *****     CREATE BACK CARD      ****** //
  // ************************************** //
  createBackCard(){
    this.backCard = new __WEBPACK_IMPORTED_MODULE_1__plastique_object_PlaneObject_js__["a" /* default */](__WEBPACK_IMPORTED_MODULE_6__Asset_js__["a" /* default */].ATLAS, __WEBPACK_IMPORTED_MODULE_6__Asset_js__["a" /* default */].CARD_BACK, VECTOR_QUALITY,VECTOR_QUALITY);
    this.backCard.pivot.x = W/2;
    this.backCard.pivot.y = H/2;
    this.cntCard.addChild(this.backCard);
    this.savedVertices = [];
    for(let i=0;i<this.backCard.vertices.length;i++){
      this.savedVertices[i] = this.backCard.vertices[i];
    }

    this.maskCard = new __WEBPACK_IMPORTED_MODULE_3__plastique_object_RectangleObject_js__["a" /* default */](W,H*2,0,5);
    this.cntCard.addChild(this.maskCard);
    this.maskCard.pivot.x = W/2;
    this.maskCard.pivot.y = H/2;
    this.maskCard.y = -H/2;

    this.backCard.mask = this.maskCard;
  }

  // ******************************* //
  // *****     PLACEMENT      ****** //
  // ******************************* //
  putAs(placement,belongToOpponent,positionId){
  //  let tmpPlacement = this.placement;
    this.placement = placement;
    this.belongToOpponent = belongToOpponent;
    this.positionId = positionId;

    if(this.placement=="out"){
      this.setHidden(true);
      this.allCnt.x = 1200;
      this.allCnt.y = 200;
      this.setUp();
    }else if(this.placement=="deck"){
      let didAction = this.setHidden(true);
      TweenLite.to(this.allCnt, .5, {x:100+positionId*3 + (this.belongToOpponent?0:600), delay:didAction?.5:0, ease: Quad.easeOut});
      TweenLite.to(this.allCnt, .5, {y:360-positionId*2 , delay:didAction?.5:0, ease: Linear.easeNone});
      this.goDownAnim(.25);
    }else if(this.placement=="battle"){
      TweenLite.to(this.allCnt, .5, {x:(this.belongToOpponent?315:485), ease: Linear.easeNone});
      TweenLite.to(this.allCnt, .5, {y:240 , ease: Quad.easeOut});
      this.setHidden(false,.5);
      TweenLite.to(this.allCnt, .33, {x:(this.belongToOpponent?(400-43):(400+43)), delay:1.5, ease: Back.easeOut});
    }
  }

  setHidden(b,d=0){
    if(this.isHidden==b) return false;
    this.isHidden = b;
    if(this.isHidden){
      this.unrevealAnim(d);
    }else{
      this.revealAnim(d);
    }
    return true;
  }

  // ********************************* //
  // *****     UPDATE CARD      ****** //
  // ********************************* //
  changeFrontCard(cardInfos){
    if(this.frontCard){
      this.cntCard.removeChild(this.frontCard);
      this.frontCard = null;
    }
    this.frontCard = new __WEBPACK_IMPORTED_MODULE_1__plastique_object_PlaneObject_js__["a" /* default */](__WEBPACK_IMPORTED_MODULE_6__Asset_js__["a" /* default */].ATLAS,__WEBPACK_IMPORTED_MODULE_6__Asset_js__["a" /* default */].CARD_PLUS_ID_PNG+"0"+(cardInfos.isOpponent?"1":"0")+"_0"+cardInfos.num+".png",VECTOR_QUALITY,VECTOR_QUALITY);
    this.frontCard.pivot.x = W/2;
    this.frontCard.pivot.y = H/2;
    this.cntCard.addChild(this.frontCard);
  }


  // ******************************** //
  // *****     ANIM GO UP      ****** //
  // ******************************** //
  setUp(){
    this.shadow.x = -30;
    this.magnetTopTop = .5;
    this.magnetTopLeft = .5;
    this.cntCard.y = -100;
  }

  goUpAnim(d=0){
    TweenLite.to(this.shadow, .5, {x:-30, ease: Quad.easeIn, delay : d});
    TweenLite.to(this, .5, {magnetTopTop:0.5, ease: Quad.easeIn, delay : d});
    TweenLite.to(this, .5, {magnetTopLeft:0.5, ease: Quad.easeIn, delay : d});
    TweenLite.to(this.cntCard, .5, {y:-100, ease: Quad.easeIn, delay : d});
  }

  goDownAnim(d=0){
    TweenLite.to(this.shadow, .5, {x:0, ease: Quad.easeOut, delay : d});
    TweenLite.to(this, .5, {magnetTopTop:0, ease: Quad.easeOut, delay : d});
    TweenLite.to(this, .5, {magnetTopLeft:0, ease: Quad.easeOut, delay : d});
    TweenLite.to(this.cntCard, .5, {y:0, ease: Quad.easeOut, delay : d});
  }

  // ************************************** //
  // *****     ANIM UNREVEAL CARD      ****** //
  // ************************************** //
  unrevealAnim(d=0){
    TweenLite.to(this.maskCard, .33, {x:-W/2, ease: Quad.easeIn, delay : d});
    TweenLite.to(this.shadow.scale, .33, {x:0, ease: Quad.easeIn, delay : d});
    TweenLite.to(this, .18, {magnetTopTop:0.33, ease: Quad.easeIn, delay : d});
    TweenLite.to(this, .18, {magnetTopLeft:0.33, ease: Quad.easeIn, delay : d});
    TweenLite.to(this, .33, {turnLeft:1, ease: Quad.easeIn, delay : d, onComplete:()=>this.endUnrevealAnim()});
  }

  endUnrevealAnim(){
    TweenLite.to(this.maskCard, .33, {x:0, ease: Quad.easeOut});
    TweenLite.to(this.shadow.scale, .33, {x:1, ease: Quad.easeOut});
    TweenLite.to(this, .33, {magnetTopTop:0, ease: Quad.easeIn});
    TweenLite.to(this, .33, {magnetTopLeft:0, ease: Quad.easeOut});
    TweenLite.to(this, .33, {turnLeft:0, ease: Quad.easeOut});
  }

  // ************************************** //
  // *****     ANIM REVEAL CARD      ****** //
  // ************************************** //
  revealAnim(d=0){
    TweenLite.to(this.maskCard, .33, {x:-W/2, ease: Quad.easeIn, delay : d});
    TweenLite.to(this.shadow.scale, .33, {x:0, ease: Quad.easeIn, delay : d});
    TweenLite.to(this, .18, {magnetTopTop:0.33, ease: Quad.easeIn, delay : d});
    TweenLite.to(this, .18, {magnetTopLeft:0.33, ease: Quad.easeIn, delay : d});
    TweenLite.to(this, .33, {turnLeft:1, ease: Quad.easeIn, delay : d, onComplete:()=>this.endRevealAnim()});
  }

  endRevealAnim(){
    TweenLite.to(this.maskCard, .33, {x:-W, ease: Quad.easeOut});
    TweenLite.to(this.shadow.scale, .33, {x:1, ease: Quad.easeOut});
    TweenLite.to(this, .33, {magnetTopTop:0, ease: Quad.easeIn});
    TweenLite.to(this, .33, {magnetTopLeft:0, ease: Quad.easeOut});
    TweenLite.to(this, .33, {turnLeft:2, ease: Quad.easeOut});
  }

  // ************************************ //
  // *****     GET/SET/UPDATE      ****** //
  // ************************************ //
  get magnetTopTop(){
    return this.magnetTopTopValue;
  }

  set magnetTopTop(r){
    this.magnetTopTopValue = r;
    this.updateCard();
  }

  get turnLeft(){
    return this.turnLeftValue;
  }

  set turnLeft(r){
    this.turnLeftValue = r;
    this.updateCard();
  }

  get magnetTopLeft(){
    return this.magnetTopLeftValue;
  }

  set magnetTopLeft(r){
    this.magnetTopLeftValue = r;
    this.updateCard();
  }

  updateCard(){
    for(let i=0;i<this.savedVertices.length;i+=2){
      let xx = this.savedVertices[i];
      let yy = this.savedVertices[i+1];

      // ** plier le coin ** //
      let fx1 = (W-xx)*this.magnetTopLeftValue*(1-yy/H)*(1-yy/H);

      // ** tourner la carte ** //
      let fx2 = (W/2-xx)*this.turnLeftValue;
      let fy2 = -((H/2-yy)*(1-Math.abs(this.turnLeftValue-1))*(xx/W))/2;

      // **plier la carte encore plus ** //
      let fy3 = -(H-yy)*this.magnetTopTopValue*(1-xx/W)*(1-yy/H)*.25;

      let fx = xx+fx1+fx2;
      let fy = yy+fy2+fy3;

      this.backCard.vertices[i] = fx;
      this.frontCard.vertices[i] = fx;
      this.backCard.vertices[i+1] = fy;
      this.frontCard.vertices[i+1] = fy;
    }
  }

}
/* harmony export (immutable) */ __webpack_exports__["a"] = CardObject;



/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__command_Command_js__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Asset_js__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__plastique_event_EventEmitter_js__ = __webpack_require__(8);




let instance = null;

const IS_LOADED = "IS_LOADED";

const TIME_DOIING = 0;
const TIME_IMPACT = 540;
const TIME_TADAA = 800;
const TIME_WHIP_BACK = 2460;
const TIME_WHIP_GO = 2760;
const TIME_END = 3120;

class SoundManager extends __WEBPACK_IMPORTED_MODULE_2__plastique_event_EventEmitter_js__["a" /* default */]{

    static get IS_LOADED(){ return IS_LOADED;}

    constructor() {
      super();
      if(!instance){
        instance = this;
        instance.aSoundToLoad = [{src:[__WEBPACK_IMPORTED_MODULE_1__Asset_js__["a" /* default */].SOUND_ATLAS],
                                  sprite:{
                                    doiing:[TIME_DOIING,TIME_IMPACT-TIME_DOIING-1],
                                    impact:[TIME_IMPACT,TIME_TADAA-TIME_IMPACT-1],
                                    tadaa:[TIME_TADAA,TIME_WHIP_BACK-TIME_TADAA-1],
                                    whip_back:[TIME_WHIP_BACK,TIME_WHIP_GO-TIME_WHIP_BACK-1],
                                    whip_go:[TIME_WHIP_GO,TIME_END-TIME_WHIP_GO]
                                  }
                                }];
      }
      return instance;
    }

    load(){
      instance.loadNextAsset();

      var command = new __WEBPACK_IMPORTED_MODULE_0__command_Command_js__["a" /* default */]();
      command.addListener(__WEBPACK_IMPORTED_MODULE_0__command_Command_js__["a" /* default */].ON_CARD, ()=>instance.playSound(__WEBPACK_IMPORTED_MODULE_1__Asset_js__["a" /* default */].SOUND_ATLAS,"whip_back"));
      command.addListener(__WEBPACK_IMPORTED_MODULE_0__command_Command_js__["a" /* default */].ON_TURN, ()=>instance.playSound(__WEBPACK_IMPORTED_MODULE_1__Asset_js__["a" /* default */].SOUND_ATLAS,"whip_go"));
      command.addListener(__WEBPACK_IMPORTED_MODULE_0__command_Command_js__["a" /* default */].ON_GAME_OVER, ()=>instance.playSound(__WEBPACK_IMPORTED_MODULE_1__Asset_js__["a" /* default */].SOUND_ATLAS,"tadaa"));
      command.addListener(__WEBPACK_IMPORTED_MODULE_0__command_Command_js__["a" /* default */].ON_NEXT, ()=>instance.playSound(__WEBPACK_IMPORTED_MODULE_1__Asset_js__["a" /* default */].SOUND_ATLAS,"impact"));
      command.addListener(__WEBPACK_IMPORTED_MODULE_0__command_Command_js__["a" /* default */].ON_IMPACT, ()=>instance.playSound(__WEBPACK_IMPORTED_MODULE_1__Asset_js__["a" /* default */].SOUND_ATLAS,"doiing"));
    }

    loadNextAsset(){
      if(instance.aSoundToLoad.length == 0){
        console.log("finitoLoadingSound!");
        instance.emit(IS_LOADED);
        return;
      }
      let asset = instance.aSoundToLoad.pop();
      instance.loadAsset(asset);
    }

    loadAsset(asset){
      console.log("loadingSound: "+asset);
      let sound = new Howl(asset);
      if(!instance.aSound) instance.aSound=[];
      instance.aSound.push({sound:sound, asset:asset.src[0]});
      sound.once('load', ()=>instance.finishLoadAsset());
    }

    playSound(asset,timing){
      (instance.getMeSound(asset)).play(timing);
    }

    getMeSound(asset){
      for(let i=0;i<instance.aSound.length;i++){
        if(instance.aSound[i].asset == asset) return instance.aSound[i].sound;
      }
    }

    finishLoadAsset(){
      instance.loadNextAsset();
    }

}
/* harmony export (immutable) */ __webpack_exports__["a"] = SoundManager;



/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Asset_js__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Config_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Main_js__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__command_Command_js__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__state_GameState_js__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__style_FontStyle_js__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__sound_SoundManager_js__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__plastique_object_RectangleObject_js__ = __webpack_require__(5);









var app;
var bg;
var cnt;
var loadingTxt;

initApp();

function initApp(){
    PIXI.utils.sayHello(PIXI.utils.isWebGLSupported()?"WebGL":"canvas");
    app = new PIXI.Application(__WEBPACK_IMPORTED_MODULE_1__Config_js__["a" /* default */].STAGE.width, __WEBPACK_IMPORTED_MODULE_1__Config_js__["a" /* default */].STAGE.height, {backgroundColor : 0x1099bb, antialias: true });
    app.view.x = getWindowSize().width/2-__WEBPACK_IMPORTED_MODULE_1__Config_js__["a" /* default */].STAGE.width/2;
    app.view.x = getWindowSize().height/2-__WEBPACK_IMPORTED_MODULE_1__Config_js__["a" /* default */].STAGE.height/2;
    __WEBPACK_IMPORTED_MODULE_1__Config_js__["a" /* default */].app = app;
    document.getElementById("pixiDiv").appendChild(app.view);
    document.getElementById("pixiDiv").style.marginLeft = -__WEBPACK_IMPORTED_MODULE_1__Config_js__["a" /* default */].STAGE.width/2+"px";
    document.getElementById("pixiDiv").style.marginTop = -__WEBPACK_IMPORTED_MODULE_1__Config_js__["a" /* default */].STAGE.height/2+"px";
    document.body.onclick=onClick;
    document.body.onkeydown=onKeyDown;
    document.body.onkeyup=onKeyUp;
    PIXI.loader
      .add(__WEBPACK_IMPORTED_MODULE_0__Asset_js__["a" /* default */].ATLAS)
      .add(__WEBPACK_IMPORTED_MODULE_0__Asset_js__["a" /* default */].PARTICULE_SNOW)
      .add(__WEBPACK_IMPORTED_MODULE_0__Asset_js__["a" /* default */].PARTICULE_STAR)
      .add(__WEBPACK_IMPORTED_MODULE_0__Asset_js__["a" /* default */].TEXT_BIGWIN_XML)
      .on("progress", loadProgressHandler)
      .load(setup);
    cnt = new PIXI.Container();
    bg = new __WEBPACK_IMPORTED_MODULE_7__plastique_object_RectangleObject_js__["a" /* default */](__WEBPACK_IMPORTED_MODULE_1__Config_js__["a" /* default */].STAGE.width,__WEBPACK_IMPORTED_MODULE_1__Config_js__["a" /* default */].STAGE.height, 0x1099bb,0);
    loadingTxt = new PIXI.Text("LOADING");
    loadingTxt.x = 345;
    loadingTxt.y = 220;
    loadingTxt.style = __WEBPACK_IMPORTED_MODULE_5__style_FontStyle_js__["a" /* default */].STYLE_LOADING;
    app.stage.addChild(cnt);
    app.stage.addChild(bg);
    app.stage.addChild(loadingTxt);
}

function getWindowSize(){
    let w = window,d = document,e = d.documentElement, g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight|| e.clientHeight|| g.clientHeight;
    return {width:x,height:y};
}

function loadProgressHandler(loader, resource) {
  console.log("loading: " + resource.url);
  console.log("progress: " + loader.progress + "%");
}

function onKeyUp(e){
    let command = new __WEBPACK_IMPORTED_MODULE_3__command_Command_js__["a" /* default */]();
    if(e.keyCode == 40) {
        command.emit(__WEBPACK_IMPORTED_MODULE_3__command_Command_js__["a" /* default */].ON_RELEASE_KEY_DOWN);
    }
}

function onKeyDown(e){
    let command = new __WEBPACK_IMPORTED_MODULE_3__command_Command_js__["a" /* default */]();
    if(e.keyCode == 37) {
        command.emit(__WEBPACK_IMPORTED_MODULE_3__command_Command_js__["a" /* default */].ON_KEY_LEFT);
    }else if(e.keyCode == 38) {
        command.emit(__WEBPACK_IMPORTED_MODULE_3__command_Command_js__["a" /* default */].ON_KEY_UP);
    }else if(e.keyCode == 39) {
        command.emit(__WEBPACK_IMPORTED_MODULE_3__command_Command_js__["a" /* default */].ON_KEY_RIGHT);
    }else if(e.keyCode == 40) {
        command.emit(__WEBPACK_IMPORTED_MODULE_3__command_Command_js__["a" /* default */].ON_KEY_DOWN);
    }
}

function onClick(){
    let command = new __WEBPACK_IMPORTED_MODULE_3__command_Command_js__["a" /* default */]();
    command.emit(__WEBPACK_IMPORTED_MODULE_3__command_Command_js__["a" /* default */].ON_CLICK);
}

function setup(){
    let sm = new __WEBPACK_IMPORTED_MODULE_6__sound_SoundManager_js__["a" /* default */]();
    sm.addListener(__WEBPACK_IMPORTED_MODULE_6__sound_SoundManager_js__["a" /* default */].IS_LOADED, ()=>startApp());
    sm.load();
}

function startApp(){
    console.log("START APP!!!");
    let mainStage = new __WEBPACK_IMPORTED_MODULE_2__Main_js__["a" /* default */](app);
    cnt.addChild(mainStage);
    TweenLite.to(bg, .25, {alpha:0, delay:.25,ease: Quad.easeIn});
    TweenLite.to(loadingTxt, .25, {alpha:0, ease: Quad.easeIn});
}


/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__object_BackgroundObject_js__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__object_GameWorldObject_js__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__object_NextButton_js__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__state_GameState_js__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__sound_SoundManager_js__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Config_js__ = __webpack_require__(1);







class Main extends PIXI.Container{

    constructor(app) {
        super();
        this.startGame();

        app.ticker.add((delta)=>this.onInterval(delta));

    }


    // ********************** //
    // *****   START   ****** //
    // ********************** //
    startGame(){
        this.initGame();
    }

    initGame(){
        this.nbTick = 0;

        let game = new __WEBPACK_IMPORTED_MODULE_3__state_GameState_js__["a" /* default */]();

        let background = new __WEBPACK_IMPORTED_MODULE_0__object_BackgroundObject_js__["a" /* default */]();
        this.addChild(background);

        let gameWorld = new __WEBPACK_IMPORTED_MODULE_1__object_GameWorldObject_js__["a" /* default */]();
        this.addChild(gameWorld);

        let nextButton = new __WEBPACK_IMPORTED_MODULE_2__object_NextButton_js__["a" /* default */]();
        this.addChild(nextButton);
        nextButton.x = __WEBPACK_IMPORTED_MODULE_5__Config_js__["a" /* default */].STAGE.width/2;
        nextButton.y = 375;
    }

    // ************************* //
    // *****   INTERVAL   ****** //
    // ************************* //

    // ************************* //
    // *****   INTERVAL   ****** //
    // ************************* //
    onInterval(delta){
      this.nbTick+=1+delta;
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Main;



/***/ }),
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__plastique_object_ParticuleObject_js__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__plastique_object_SpriteObject_js__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Config_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__style_FontStyle_js__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Asset_js__ = __webpack_require__(0);







class BackgroundObject extends PIXI.Container{

  constructor() {
    super();
    this.createBg();
    this.createParticule();
    this.createAndStartAnimationText();
    this.createInstructions();
  }

  createBg(){
    let degrade = new __WEBPACK_IMPORTED_MODULE_1__plastique_object_SpriteObject_js__["a" /* default */](__WEBPACK_IMPORTED_MODULE_4__Asset_js__["a" /* default */].ATLAS, __WEBPACK_IMPORTED_MODULE_4__Asset_js__["a" /* default */].BG);
    this.addChild(degrade);
  }

  createParticule(){
    let particule = new __WEBPACK_IMPORTED_MODULE_0__plastique_object_ParticuleObject_js__["a" /* default */](__WEBPACK_IMPORTED_MODULE_4__Asset_js__["a" /* default */].ATLAS, __WEBPACK_IMPORTED_MODULE_4__Asset_js__["a" /* default */].STAR, __WEBPACK_IMPORTED_MODULE_4__Asset_js__["a" /* default */].PARTICULE_SNOW);
    this.addChild(particule);
    particule.startParticule();
  }


  createAndStartAnimationText(){
    let totalW=0;
    this.cntLetter = new PIXI.Container();
    this.addChild(this.cntLetter);

    let text = "war in the zoo!";
    let aText = text.split("");

    for(let i=0;i<aText.length;i++){
      let w = this.createOneLetter(aText[i],totalW,i);
      totalW+=w;
    }
    this.cntLetter.x = __WEBPACK_IMPORTED_MODULE_2__Config_js__["a" /* default */].STAGE.width/2 - totalW/2;
  }

  createOneLetter(letter,xx,d){
    if(letter == " ") return 12;
    let bitmapText = new PIXI.extras.BitmapText(letter, {font: "35px "+__WEBPACK_IMPORTED_MODULE_4__Asset_js__["a" /* default */].TEXT_BIGWIN, align: "left"});
    this.cntLetter.addChild(bitmapText);
    bitmapText.x = xx;
    bitmapText.y = -50;

    TweenLite.to(bitmapText, 1, {y:33, ease: Elastic.easeOut, delay : d*.1+.5});
    return bitmapText.textWidth;
  }

  createInstructions(){
      let txt = new PIXI.Text("Each player got cards from 1 to 5. The strongest card wins, except the 1 which beats the 5. When you win against a card, you take it in your deck. Take all the cards of your opponent to win!");
      txt.x = __WEBPACK_IMPORTED_MODULE_2__Config_js__["a" /* default */].STAGE.width/2 - __WEBPACK_IMPORTED_MODULE_3__style_FontStyle_js__["a" /* default */].STYLE_TXT.wordWrapWidth/2 ;
      txt.y = 100;
      txt.alpha = 0;
      txt.style = __WEBPACK_IMPORTED_MODULE_3__style_FontStyle_js__["a" /* default */].STYLE_TXT;
      this.addChild(txt);
      TweenLite.to(txt, 1, {y:80, alpha :1, ease: Quad.easeOut, delay : 2});
  }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = BackgroundObject;



/***/ }),
/* 14 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__plastique_object_ParticuleObject_js__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__plastique_object_SpriteObject_js__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__plastique_object_RectangleObject_js__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__CardObject_js__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Config_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__command_Command_js__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__state_GameState_js__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__style_FontStyle_js__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__Asset_js__ = __webpack_require__(0);











class GameWorldObject extends PIXI.Container{

  constructor() {
    super();
    this.resultat = "start";
    this.createCrown();
    this.distributeAllCard();
  }


  // *************************** //
  // *****     CROWN      ****** //
  // *************************** //
  createParticule(){
    if(this.particule){
      this.removeChild(this.particule);
      this.particule = null;
    }
    (new __WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */]()).emit(__WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */].ON_IMPACT);
    this.particule = new __WEBPACK_IMPORTED_MODULE_0__plastique_object_ParticuleObject_js__["a" /* default */](__WEBPACK_IMPORTED_MODULE_8__Asset_js__["a" /* default */].ATLAS, __WEBPACK_IMPORTED_MODULE_8__Asset_js__["a" /* default */].STAR, __WEBPACK_IMPORTED_MODULE_8__Asset_js__["a" /* default */].PARTICULE_STAR,.5);
    this.addChild(this.particule);
    this.particule.x = 400;
    this.particule.y = 220;
    this.particule.startParticule();
  }

  // *************************** //
  // *****     CROWN      ****** //
  // *************************** //
  createCrown(){
    this.crown = new __WEBPACK_IMPORTED_MODULE_1__plastique_object_SpriteObject_js__["a" /* default */](__WEBPACK_IMPORTED_MODULE_8__Asset_js__["a" /* default */].ATLAS, __WEBPACK_IMPORTED_MODULE_8__Asset_js__["a" /* default */].CROWN);
    this.addChild(this.crown);
    this.crown.alpha = 0;
    this.crown.scale.x = this.crown.scale.y = .5;
  }

  hideCrown(){
    TweenLite.to(this.crown, .25, {alpha:0, y:130 , ease: Quad.easeOut});
  }

  showCrown(isOpponent,d){
    this.crown.x = isOpponent?343:429;
    this.crown.y = 130;
    this.crown.alpha = 0;
    TweenLite.to(this.crown, .25, {alpha:1, y:150 , delay:d, ease: Back.easeOut});
  }

  // **************************************** //
  // *****     INIT // GIVE CARDS      ****** //
  // **************************************** //
  distributeAllCard(){
    this.aCard = [];

    let gameState = new __WEBPACK_IMPORTED_MODULE_6__state_GameState_js__["a" /* default */]();

    for(let i=0;i<gameState.deckOpponent.length;i++){
      this.distributeCard(i,gameState.deckOpponent[i],i*.25);
    }

    for(let i=0;i<gameState.deckPlayer.length;i++){
      this.distributeCard(i,gameState.deckPlayer[i],i*.25+.12);
    }
    setTimeout(function(){
      (new __WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */]()).emit(__WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */].IS_READY);
    },(5*.25+.12+1)*1000);

    (new __WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */]()).addListener(__WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */].ON_NEXT,()=>this.onNext());
  }

  distributeCard(i,cardInfos, delay){
    setTimeout(()=>(new __WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */]()).emit(__WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */].ON_CARD),delay*1000+350);
    let card = new __WEBPACK_IMPORTED_MODULE_3__CardObject_js__["a" /* default */](cardInfos);
    this.addChild(card);
    this.aCard.push(card);
    card.putAs("out",cardInfos.isOpponent,i);
    setTimeout(function(){
      card.putAs("deck",cardInfos.isOpponent,i);
    },delay*1000);
  }

  // ************************************ //
  // *****     ON NEXT BUTTON      ****** //
  // ************************************ //
  onNext(){
    (new __WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */]()).emit(__WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */].WAIT_PLEASE);
    if(this.resultat == "start"){
      this.showNextCards();
      return;
    }

    let gameState = new __WEBPACK_IMPORTED_MODULE_6__state_GameState_js__["a" /* default */]();
    let card1 = this.getCard(gameState.removeCardOpponent());
    let card2 = this.getCard(gameState.removeCardPlayer());

    if(this.resultat == "equals"){
      this.sentToDeck(true,card1);
      this.sentToDeck(false,card2);
    }else if(this.resultat == "playerwin"){
      this.hideCrown();
      this.sentToDeck(false,card1,card2);
    }else if(this.resultat == "opponentwin"){
      this.hideCrown();
      this.sentToDeck(true,card1,card2);
    }
    this.reOrder();
    setTimeout(()=>(new __WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */]()).emit(__WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */].ON_CARD),600);
    setTimeout(()=>(new __WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */]()).emit(__WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */].ON_CARD),630);

    setTimeout(()=>this.showNextCards(),1200);
  }

  reOrder(){
    for(let i=0;i<this.aCard.length;i++){
      this.removeChild(this.aCard[i]);
    }

    let gameState = new __WEBPACK_IMPORTED_MODULE_6__state_GameState_js__["a" /* default */]();
    for(let i=0;i<gameState.deckOpponent.length;i++){
      let card = this.getCard(gameState.deckOpponent[i]);
      this.addChild(card);
    }

    for(let i=0;i<gameState.deckPlayer.length;i++){
      let card = this.getCard(gameState.deckPlayer[i]);
      this.addChild(card);
    }
  }

  showNextCards(){
    let gameState = new __WEBPACK_IMPORTED_MODULE_6__state_GameState_js__["a" /* default */]();
    if(gameState.isFinito()){
      this.gameOver();
      return;
    }

    setTimeout(()=>(new __WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */]()).emit(__WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */].ON_TURN),600);
    setTimeout(()=>(new __WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */]()).emit(__WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */].ON_TURN),630);
    setTimeout(()=>(new __WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */]()).emit(__WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */].ON_CARD),100);
    setTimeout(()=>(new __WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */]()).emit(__WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */].ON_CARD),130);
    let card1 = this.getCard(gameState.getNextCardOpponent());
    let card2 = this.getCard(gameState.getNextCardPlayer());
    card1.putAs("battle",true,0);
    card2.putAs("battle",false,0);

    setTimeout(()=>this.createParticule(),1600);
    setTimeout(()=>this.onNextFinito(),2200);

    if(card1.infos.num == card2.infos.num){
      this.resultat = "equals";
      //equals
    }else if(card1.infos.num == 5 && card2.infos.num == 1 || ((card1.infos.num < card2.infos.num) && !(card1.infos.num == 1 && card2.infos.num == 5))){
      //playerwin
      this.resultat = "playerwin";
      this.showCrown(false,2.3);
    }else {
      //opponentwin
      this.resultat = "opponentwin";
      this.showCrown(true,2.3);
    }
  }

  onNextFinito(){
    (new __WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */]()).emit(__WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */].IS_READY);
  }

  // ********************************** //
  // *****     SEND TO DECK      ****** //
  // ********************************** //
  sentToDeck(isOpponent,card1,card2=null){
    let gameState = new __WEBPACK_IMPORTED_MODULE_6__state_GameState_js__["a" /* default */]();
    let d = 1;

    // move cards
    card1.putAs("deck",isOpponent,0);
    if(card2){
      card2.putAs("deck",isOpponent,1);
      d = 2;
    }

    // reorder
    let deck = gameState.deckPlayer;
    if(isOpponent) deck = gameState.deckOpponent;
    for(let i=0;i<deck.length;i++){
      let card = this.getCard(deck[i]);
      card.putAs("deck",isOpponent,i+d);
    }

    // update le deck dans le gamestate
    if(card2){
      if(!isOpponent) gameState.addToPlayerDeck(card2.infos);
      if(isOpponent) gameState.addToOpponentDeck(card2.infos);
    }

    if(!isOpponent) gameState.addToPlayerDeck(card1.infos);
    if(isOpponent) gameState.addToOpponentDeck(card1.infos);
  }

  // ************************* //
  // *****     FCT      ****** //
  // ************************* //
  getCard(cardinfos){
    for(let i=0;i<this.aCard.length;i++){
      if(this.aCard[i].infos.isOpponent == cardinfos.isOpponent && this.aCard[i].infos.num == cardinfos.num) return this.aCard[i];
    }
  }

  gameOver(){
    console.log("GAMEOVER!");
    setTimeout(()=>(new __WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */]()).emit(__WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */].ON_GAME_OVER),600);
    let gameState = new __WEBPACK_IMPORTED_MODULE_6__state_GameState_js__["a" /* default */]();
    let b = gameState.doYouWin();
    let txt= b?"you win!!!!":"you lose!!!!";

    let rect = new __WEBPACK_IMPORTED_MODULE_2__plastique_object_RectangleObject_js__["a" /* default */](__WEBPACK_IMPORTED_MODULE_4__Config_js__["a" /* default */].STAGE.width,__WEBPACK_IMPORTED_MODULE_4__Config_js__["a" /* default */].STAGE.height,0,0);
    this.addChild(rect);
    rect.alpha=0;

    let bitmapText = new PIXI.extras.BitmapText(txt, {font: "50px "+__WEBPACK_IMPORTED_MODULE_8__Asset_js__["a" /* default */].TEXT_BIGWIN, align: "left"});
    this.addChild(bitmapText);
    let colorMatrix = new PIXI.filters.ColorMatrixFilter();
    colorMatrix.hue(b?-85:135);
    bitmapText.filters = [colorMatrix];
    bitmapText.x = __WEBPACK_IMPORTED_MODULE_4__Config_js__["a" /* default */].STAGE.width/2-bitmapText.textWidth/2;
    bitmapText.y = -75;

    TweenLite.to(rect, .5, {alpha:.88, ease: Quad.easeOut});
    TweenLite.to(bitmapText, 1, {y:200, ease: Bounce.easeOut, delay : .5});
  }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = GameWorldObject;



/***/ }),
/* 15 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class PlaneObject extends PIXI.mesh.Plane{

  constructor(fileNameAtlas,fileNameImg,xx,yy) {
    super(PIXI.loader.resources[fileNameAtlas].textures[fileNameImg],xx,yy);
  }

}
/* harmony export (immutable) */ __webpack_exports__["a"] = PlaneObject;



/***/ }),
/* 16 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__plastique_object_ParticuleObject_js__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__plastique_object_SpriteObject_js__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__plastique_object_RectangleObject_js__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__CardObject_js__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Config_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__command_Command_js__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__state_GameState_js__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__style_FontStyle_js__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__Asset_js__ = __webpack_require__(0);










const W = 130;
const H = 50;

class NextButton extends PIXI.Container{
    static get W(){ return W;}
    constructor() {
      super();

      this.cnt = new PIXI.Container();
      this.addChild(this.cnt);

      let bg = new __WEBPACK_IMPORTED_MODULE_2__plastique_object_RectangleObject_js__["a" /* default */](W,H,0xffffff,10);
      this.cnt.addChild(bg);
      this.cnt.pivot.x = W/2;
      this.cnt.pivot.y = H/2;

      let txt = new PIXI.Text("NEXT!");
      txt.x = 30;
      txt.y = 12;
      txt.style = __WEBPACK_IMPORTED_MODULE_7__style_FontStyle_js__["a" /* default */].STYLE_BTN;
      this.cnt.addChild(txt);

      this.colorMatrix = new PIXI.filters.ColorMatrixFilter();
      this.cnt.filters = [this.colorMatrix];

      this.setActive(false);

      let c = new __WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */]();

      this.click = function (e) {
          c.emit(__WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */].ON_NEXT);
      }

      c.addListener(__WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */].IS_READY,()=>this.setActive(true));
      c.addListener(__WEBPACK_IMPORTED_MODULE_5__command_Command_js__["a" /* default */].WAIT_PLEASE,()=>this.setActive(false));

      this.mouseover = function (e) {
        if(this.isActive) TweenLite.to(this.cnt.scale, .5, {y:1.05,x:1.05, ease: Quad.easeOut});
      }

      this.mouseout = function (e) {
        if(this.isActive) TweenLite.to(this.cnt.scale, .5, {y:1,x:1, ease: Quad.easeOut});
      }

    }

    setActive(b){
      this.isActive = b
      this.interactive = b;
      this.buttonMode = b;
      this.colorMatrix.saturate(b?0:-1);
      this.cnt.alpha= (b?1:.5);
      TweenLite.to(this.cnt, .5, {y:(b?0:100), ease: Quad.easeOut});
    }

}
/* harmony export (immutable) */ __webpack_exports__["a"] = NextButton;



/***/ })
/******/ ]);